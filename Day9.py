from collections import deque

input = (419,7105200) #player,marbles
marbles = deque([2,1,0])
currentPlayer = 3
nextMarble = 3
playerScore = dict()
for p in range(input[0]):
    playerScore[p+1] = 0

def rotateright(index):
    for i in range(index):
        elem = marbles.popleft()
        marbles.append(elem)

def rotateleft(index):
    for i in range(index):
        elem = marbles.pop()
        marbles.appendleft(elem)

while input[1] > nextMarble:
    if nextMarble % 23 == 0:
        rotateleft(7)
        removedMarble = marbles.popleft()
        playerScore[currentPlayer] += nextMarble + removedMarble
        nextMarble += 1
    else:
        rotateright(2)
        marbles.appendleft(nextMarble)
        nextMarble += 1
    currentPlayer = (currentPlayer % input[0]) + 1

print(max(playerScore.values()))