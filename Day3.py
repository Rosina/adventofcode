def printArray(array):
    for row in array:
        for e in row:
            print("{:2} ".format(e), end='')
        print()


input = open("inputday3", "r")
spec = []
for line in input:
    tmp = line.split(" ")
    pos = tmp[2].split(",")
    pos[1] = pos[1].split(":")[0]
    size = tmp[3].split("x")
    id = int(tmp[0].split('#')[1])
    spec.append(((int(pos[0]),int(pos[1]), int(size[0]), int(size[1]), id)))
print(spec)
fabric = [[0] * 1000 for i in range(1000)]

for elem in spec:
    for x in range(elem[0],elem[0]+elem[2]):
        for y in range(elem[1],elem[1]+elem[3]):
            fabric[x][y] += 1
#printArray(fabric)

count = 0
for x in range(1000):
    for y in range(1000):
        if fabric[x][y] > 1:
            count += 1
print("overlapping inches: " + str(count))
outputID = 0
for elem in spec:
    intact = True
    for x in range(elem[0],elem[0]+elem[2]):
        for y in range(elem[1],elem[1]+elem[3]):
            if fabric[x][y] > 1:
                intact = False
    if intact:
        outputID = elem[4]
print("ID of only not overlapping piece: " + str(outputID))