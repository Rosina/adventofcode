
from datetime import datetime

def keymethod(elem):
    return elem[0]

def sleeptime(times):
    sleep = 0
    for i in range(0,len(times),2):
        sleep += times[i+1] - times[i]
    return sleep

input = open("inputday4", "r")
list = []
for line in input:
    tmp = line.split("]")
    time = tmp[0]
    rest = tmp[1]
    datetime_object = datetime.strptime(time, '[%Y-%m-%d %H:%M')
    list.append((datetime_object, rest))

list.sort(key=keymethod)

guards = {}
guard = ''
time = 0
interval = []
for elem in list:
    if elem[1][7] == '#':
        if guard != '':
            guards[guard].append(interval)
            interval = []
        guard = elem[1].split(" ")[2]
        if guard not in guards:
            guards[guard] = [[]]
    else:
        time = elem[0].minute
        interval.append(time)

print("Input:")
for x, y in guards.items():
    print(x, y)


def part1():
    print("Part 1:")
    longestSleeper = ['',0,[]]
    for x, y in guards.items():
        sleep = 0
        for s in y:
            if len(s) > 0:
                sleep += sleeptime(s)

        if sleep > longestSleeper[1]:
            longestSleeper[0] = x
            longestSleeper[1] = sleep
            longestSleeper[2] = y

    #print("longest sleeper: " + str(longestSleeper))
    #3167

    originalIntervals = [[]]
    for elem in longestSleeper[2]:
        if len(elem) > 2:
            #print(elem)
            tmp = elem
            originalIntervals.append(tmp[0:2])
            originalIntervals.append(tmp[2:4])
            if len(tmp) > 4:
                originalIntervals.append(tmp[4:6])
        else:
            originalIntervals.append(elem)

    while [] in originalIntervals:
        originalIntervals.remove([])

    count = [0 for i in range(59)]
    for inter in originalIntervals:
        for m in range(inter[0],inter[1]):
            count[m] += 1

    threshold = 0
    out = 0

    for i in count:
        if i > threshold:
            out = count.index(i)
            threshold = i
    print("longest sleeper: " + str(longestSleeper[0]))
    print("most frequently asleep at minute minute:" + str(out))
    print("Nr of times asleep at that minute:" + str(count[out]))

def part2():
    print("Part 2:")
    output = [] #(guard,minute, times asleep)
    for x, y in guards.items():
        intervals = [[]]
        for elem in y:
            if len(elem) > 2:
                tmp = elem
                intervals.append(tmp[0:2])
                intervals.append(tmp[2:4])
                if len(tmp) > 4:
                    intervals.append(tmp[4:6])
            else:
                if elem != []:
                    intervals.append(elem)
        intervals.remove([])

        count = [0 for i in range(59)]
        for inter in intervals:
            for p in range(inter[0], inter[1]):
                count[p] += 1

        threshold = 0
        out = 0
        for i in count:
            if i > threshold:
                out = count.index(i)
                threshold = i
        output.append((x,out, count[out]))

    threshold = 0
    solution = []
    for i in output:
        if i[2] > threshold:
            threshold = i[2]
            solution = i
    print("Guard ID: " + str(solution[0]) + " most fequently asleep at minute:" + str(solution[1]))

part1()
print()
part2()