input = open("inputday6", "r")

def printArray(array):
    for row in array:
        for e in row:
            print("{:2} ".format(e), end='')
        print()

def closest(x,y,coordinates):
    distance = []
    for xi, yi in coordinates:
        distance.append(abs(xi - x) + abs(yi -y))
    minD = min(distance)
    index = distance.index(minD)
    distance.remove(minD)

    if minD in distance:
        return '.'
    else:
        return index


def distantsum(x,y,coordinates):
    distance = []
    for xi, yi in coordinates:
        distance.append(abs(xi - x) + abs(yi -y))
    sumD = sum(distance)
    if sumD < 10000:
        return 1
    else:
        return '.'


coordinates = []
maxX = 0

maxY = 0

for line in input:
    c = line.rstrip().split(", ")
    x = int(c[0])
    if x > maxX:
        maxX = x

    y = int(c[1])
    if y > maxY:
        maxY = y
    coordinates.append((x , y))

fieldpart1 = [['.']*(maxY+1) for i in range(maxX+1)]
fieldpart2 = [['.']*(maxY+1) for i in range(maxX+1)]

number = 0
for c in coordinates:
    fieldpart1[c[0]][c[1]] = str(number) + "*"
    fieldpart2[c[0]][c[1]] = str(number) + "*"
    number += 1

for x in range(len(fieldpart1)):
    for y in range(len(fieldpart1[0])):
        point = fieldpart1[x][y]
        if isinstance(point, str) :#and point[0] == '.':
            fieldpart2[x][y] = distantsum(x,y,coordinates)#closest(x,y,coordinates)
            if point[0] == '.':
                fieldpart1[x][y] = closest(x,y,coordinates)

size1 = [0 for i in range(len(coordinates))]
size2 = [0 for i in range(len(coordinates))]
border = [0 for i in range(len(coordinates))]

for x in range(len(fieldpart1)):
    for y in range(len(fieldpart1[0])):
        point1 = fieldpart1[x][y]
        point2 = fieldpart2[x][y]
        if isinstance(point1, int):
            size1[point1] += 1
            if x == 0 or x == len(fieldpart1) or y == 0 or y == len(fieldpart1[0]):
                border[point1] = 1
        if isinstance(point2, int):
            size2[point2] += 1

filterSize = []

for i in range(len(size1)):
    if border[i] == 0:
        filterSize.append(size1[i])

print("Solution part 1: " + str(max(filterSize)+1))
print("Solution part 2: " + str(max(size2)))
