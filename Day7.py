import string

def input():
    input = open("inputday7", "r")

    names =  string.ascii_uppercase
    steps = dict()
    prequisits = dict()
    seconds = dict()

    for i in range(len(names)):
        a = names[i]
        steps[a] = []
        prequisits[a] = []
        seconds[a] = i+1+60

    for line in input:
        tmp = line.split(" ")
        steps[tmp[1]].append(tmp[7])
        prequisits[tmp[7]].append(tmp[1])
    input.close()
    return steps, prequisits, seconds, names


def example():
    steps = dict()
    prequisits = dict()
    seconds = dict()

    steps['C'] = ['A', 'F']
    steps['A'] = ['B', 'D']
    steps['B'] = ['E']
    steps['D'] = ['E']
    steps['E'] = []
    steps['F'] = ['E']

    prequisits['A'] = ['C']
    prequisits['B'] = ['A']
    prequisits['C'] = []
    prequisits['D'] = ['A']
    prequisits['E'] = ['B','D','F']
    prequisits['F'] = ['C']

    seconds['A'] = 1
    seconds['B'] = 2
    seconds['C'] = 3
    seconds['D'] = 4
    seconds['E'] = 5
    seconds['F'] = 6

    names = "ABCDE"

    return steps, prequisits, seconds, names


def start(steps, names):
    start = []

    for x in steps.values():
        for a in x:
            names = names.replace(a,"")

    for a in names:
        start.append(a)
    start.sort()
    return start

def part2(steps, prequisits, seconds, start):
    time = 0
    worker = ['.' for i in range(5)]
    done = ""
    while len(done) < len(steps.keys()):
        while '.' in worker and start:
            step = start[0]
            start.remove(step)
            worker.remove('.')
            worker.append(step)
        time += 1
        for j in range(len(worker)):
            if worker[j] != '.':
                step = worker[j]
                seconds[step] -= 1
                if seconds[step] == 0:
                    done += worker[j]
                    worker[j] = '.'
                    for i in steps[step]:
                        if i not in start and i not in done and i not in worker:
                            if prequisits[i]:
                                prequisits[i].remove(step)
                            if not prequisits[i]:
                                start.append(i)

        start.sort()
    print("order: "+ done)
    print("Time needed: " + str(time))

def part1(steps, prequisits, start):
    order = ""
    while start:
        step = start[0]
        start.remove(step)
        order += step
        for i in steps[step]:
            if i not in start and i not in order:
                if prequisits[i]:
                    prequisits[i].remove(step)
                if not prequisits[i]:
                    start.append(i)
        start.sort()

    print("solution: " + order)


steps, prequisits, seconds, names = input()
s = start(steps,names)
print("Part 1:")
part1(steps,prequisits,s)
steps, prequisits, seconds, names = input()
s = start(steps,names)
print("Part 2:")
part2(steps,prequisits,seconds,s)