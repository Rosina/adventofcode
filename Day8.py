from sys import setrecursionlimit

setrecursionlimit(100000)

inputFile = open("inputday8", "r")

input = []

for line in inputFile:
    tmp = line.split(' ')
    for i in tmp:
        input.append(int(i))

metadatasum = 0
index = 0
nrofmetadata = 0
nrofchildren = 0

def metasum(metadatasum, index, input):
    nrofchildren = input[index]
    nrofmetadata = input[index+1]
    index += 2
    step = 0
    while step < nrofchildren:
        metadatasum, index = metasum(metadatasum, index,input)
        step +=1

    step = 0

    while step < nrofmetadata:
        metadatasum += input[index]
        index +=1
        step +=1
    return (metadatasum, index)

def valuenodes(index,input):
    nrofchildren = input[index]
    nrofmetadata = input[index + 1]
    metaentries = []
    childvalues = []

    if nrofchildren == 0:
        value, index = metasum(0, index, input)
        return value, index
    else:
        step = 0
        index += 2
        while step < nrofchildren and index < len(input):
            value, index = valuenodes(index,input)
            childvalues.append(value)
            step += 1

    step = 0

    while step < nrofmetadata:
        metaentries.append(input[index])
        index += 1
        step += 1
    value = 0
    for i in metaentries:
        if i <= nrofchildren:
            value += childvalues[i-1]
    return value, index

example = [2, 3, 0, 3, 10, 11 ,12 ,1 ,1, 0, 1 ,99, 2, 1, 1, 2]

metadatasum = metasum(metadatasum,index,input)
print("Solution part 1: " + str(metadatasum[0]))
value = valuenodes(0,input)
print("Solution part 2: " + str(value[0]))