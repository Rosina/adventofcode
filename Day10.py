inputFile = open("inputday10", "r")
input = [[]]
minX = 0
maxX = 0
minY = 0
maxY = 0

def move(zeropoint,x):
    return x+(zeropoint*-1)


def drawSpace(space,input,minX,minY):
    for index, i in enumerate(input):
        pos = i[0]
        x = move(minX,pos[0])
        y = move(minY,pos[1])

        if x < len(space) and y < len(space[0]) and x >= 0 and y >= 0:
            space[x][y] = '#'
    return space

def printArray(array):
    for row in array:
        for e in row:
            print("{:2} ".format(e), end='')
        print()

def updateInput(input,steps):
    newInput = [[]]
    for index,elem in enumerate(input):
        pos = elem[0]
        v = elem[1]
        x = pos[0]+(steps*v[1])
        y = pos[1]+(steps*v[0])
        newInput.append([(x,y),v])
    newInput.remove([])
    return newInput

def ranges(input):
    minX = 300
    maxX = 0
    minY = 300
    maxY = 0
    for elem in input:
        x = elem[0][0]
        y = elem[0][1]
        if x < minX:
            minX = x
        if x > maxX:
            maxX = x
        if y < minY:
            minY = y
        if y > maxY:
            maxY = y
    return minX, maxX,minY,maxY

def returnX(i):
    return i[0][0]

def returnY(i):
    return i[0][1]

def findConnected(input):
    connected = False
    hasN = []
    for index1, elem in enumerate(input):

        x = elem[0][0]
        y = elem[0][1]

        n = False

        for i2 in range(len(input)):
            elem2 = input[i2]
            x2 = elem2[0][0]
            y2 = elem2[0][1]
            if not (x == x2 and y == y2):
                if (x == x2 or x == (x2+1) or x == (x2-1)):
                    if (y == y2 or y == (y2+1) or y == (y2-1)) :
                        n = True
        hasN.append(n)
    if not False in hasN:
        connected = True
    return connected


for line in inputFile:
    line = line.replace("position=<","")
    line = line.replace("velocity=<","")
    line = line.rstrip()
    line = line.replace(">","")
    line = line.replace(", ", " ")
    line = line.split(" ")
    while '' in line:
        line.remove('')
    x = line[1]
    y = line[0]
    input.append([(int(x),int(y)),(int(line[2]),int(line[3]))])
    if int(x) < minX:
        minX = int(x)
    if int(x) > maxX:
        maxX = int(x)
    if int(y) < minY:
        minY = int(y)
    if int(y) > maxY:
        maxY = int(y)
input.remove([])

skip = 10123
input = updateInput(input, skip)

for i in range(5):
    input = updateInput(input, 1)

    if findConnected(input):
        minX, maxX, minY, maxY = ranges(input)
        space = [['.'] * (maxY + (minY*-1) +1) for i in range(maxX + (minX*-1)+1)]
        space = drawSpace(space,input,minX,minY)
        printArray(space)
        print("Seconds: " + str(skip + i + 1))