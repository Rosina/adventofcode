
def part2():
    ids = []
    common = {}

    for line in input:
        ids.append(line)

    for x in range(len(ids)):
        first = ids[x]
        for y in range(x+1,len(ids)):
            second = ids[y]
            commonString = ""
            for i in range(len(second)):
                if first[i] == second[i]:
                    commonString += first[i]
            common[commonString] = len(commonString)

    values = common.items()
    for item in values:
        if item[1] == 26:
            print(item)

def part1():
    counts = []

    for line in input:
        id = set()
        dictionary = {}
        for char in line:
            if char not in id:
                id.add(char)
            else:
                if char in dictionary:
                    dictionary[char] += 1
                else:
                    dictionary[char] = 2
        #print("id = " + line)
        #print(dictionary)
        counts.append(dictionary)

    twoTimes = 0
    threeTimes = 0

    for dictionary in counts:
        if 2 in dictionary.values():
            twoTimes += 1
        if 3 in dictionary.values():
            threeTimes += 1

    print("two: " + str(twoTimes))
    print("three: " + str(threeTimes))
    print("checksum: " + str(twoTimes*threeTimes))

print("Part 1:")
input = open("inputday2", "r")
part1()
input.close()
print("\nPart 2:")
input = open("inputday2", "r")
part2()
input.close()