import string

def part1():
    input = open("inputday5", "r")
    inputstring = ""
    for line in input:
        inputstring = line.rstrip()

    changed = True
    reactions = []
    xpol =  string.ascii_uppercase
    ypol =  string.ascii_lowercase

    for x in range(len(xpol)):
        r = xpol[x]+ypol[x]
        reactions.append(r)
        r = ypol[x]+xpol[x]
        reactions.append(r)

    while changed:
        test = inputstring
        for r in reactions:
            inputstring = inputstring.replace(r,"")

        if test == inputstring:
            changed = False

    print(len(inputstring))
    input.close()

def reaction(polymeer):
    polymeer = polymeer.rstrip()
    changed = True
    reactions = []
    xpol = string.ascii_uppercase
    ypol = string.ascii_lowercase

    for x in range(len(xpol)):
        r = xpol[x] + ypol[x]
        reactions.append(r)
        r = ypol[x] + xpol[x]
        reactions.append(r)

    while changed:
        test = polymeer
        for r in reactions:
            polymeer = polymeer.replace(r, "")

        if test == polymeer:
            changed = False
    return len(polymeer)


def part2():
    input = open("inputday5", "r")
    inputstring = ""
    for line in input:
        inputstring = line

    units = []

    xpol = string.ascii_uppercase
    ypol = string.ascii_lowercase

    for x in range(len(xpol)):
        r = xpol[x] + ypol[x]
        units.append(r)

    reactionlen = []

    for x in units:
        tmp = inputstring
        for u in x:
            tmp = tmp.replace(u,"")
        reactionlen.append(reaction(tmp))
    print(min(reactionlen))
    input.close()

print("Part 1:")
part1()
print("Part 2:")
part2()