serialNr = 8199

def printArray(array):
    for row in array:
        for e in row:
            print("{:2} ".format(e), end='')
        print()

def rackID(x):
    return x+10

def powerLevel(id, y):
    return id*y

def addSerial(pLevel, s):
    return pLevel + s

def multID(p, id):
    return p*id

def getDigit(i):
    return int(str(i)[len(str(i))-3])

def getPower(x,y,grid):
    if x+2 < 300 and y+2 < 300:
        return grid[y][x] + grid[y+1][x] + grid[y+2][x] + grid[y][x+1] + grid[y][x+2] + grid[y+1][x+1] + grid[y+1][x+2] + grid[y+2][x+1] + grid[y+2][x+2]
    else:
        return 0

def getPowerPart2(x,y,grid):
    size = 0
    maxPower = 0
    maxSize = 0
    power = 0
    while x+size < 300 and y+size < 300:
        for s1 in range(size):
            power += grid[y + s1][x+size]
        for s2 in range(size):
            power += grid[y+size][x + s2]
        power += grid[y+size][x+size]
        if power > maxPower:
            maxPower = power
            maxSize = size
        size += 1
    return (maxPower,maxSize)


def fillGrid(serial):
    grid = [[0]*300 for i in range(300)]

    for y in range(1,301):
        for x in range(1,301):
            id = rackID(x)
            grid[y-1][x-1] = getDigit(multID(addSerial(powerLevel(id,y),serial),id))-5
    return grid

def part1(grid):
    maxPower = 0
    pos = 0
    maxsize = 0

    for y in range(1,301):
        for x in range(1,301):
            power = getPower(x - 1, y - 1, grid)
            if power > maxPower:
                maxPower = power
                pos = (x,y)
    print("Power: " + str(maxPower))
    print(pos)

def part2(grid):
    maxPower = 0
    pos = 0
    maxsize = 0

    for y in range(1,301):
        for x in range(1,301):
            power,size = getPowerPart2(x-1, y-1, grid)
            if power > maxPower:
                maxPower = power
                pos = (x,y)
                maxsize = size
    print("Power: " + str(maxPower))
    print("Size: " + str(maxsize + 1))
    print(pos)

print("Part 1:")
grid = fillGrid(serialNr)
part1(grid)

print()
print("Part 2:")
part2(grid)